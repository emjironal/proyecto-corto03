const express = require('express')
const path = require('path');
const csv = require('csvtojson')

const app = express()
const PORT = process.env.PORT || 3000

app.set('view engine', 'ejs')
app.set('views', __dirname + '/views'); 
app.use(express.static(path.join(__dirname, 'public')))

function getMain(req, res)
{
	res.render("pages/index.ejs") 
}

function getHappiness(req, res)
{
    csv()
        .fromFile('./public/assets/WorldHappiness/2019.csv')
        .then((json)=>
        {
            res.send(json)
        })
}

app.get('/', getMain)
app.get('/getHappiness', getHappiness)

app.listen(PORT, ()=>
{
    console.log('Server started at: http://localhost:'+ PORT)
}) //end listen
